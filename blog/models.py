from django.db import models

# Create your models here.
class Post(models.Model):
    titulo = models.CharField(max_length=100, blank = True)
    texto = models.TextField()
    data = models.CharField(max_length=15, blank = True)

    def __str__(self) -> str:
        return self.titulo